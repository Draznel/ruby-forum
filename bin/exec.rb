require './lib/database'
require './lib/forum'
require './lib/user'

STDOUT.sync = true
@current_user = nil
@users = nil

def start
  @users = Database.load(User)
  puts 'This is an interactive ruby testing for Forum'
  main_menu
end

def main_menu
  selection = nil
  while selection != 6
    text = "\n###Main menu###\n" +
        'Current user: ' + current_user_name + "\n" +
        "1. Select user\n" +
        "2. Edit current user\n" +
        "3. Create a new user\n" +
        "4. Delete current user\n" +
        "5. Save\n" +
        "6. Exit\n" +
        'Select an option: '
    selection = get_selection(text, 1, 6)

    case selection
      when 1
        select_user
      when 2
        edit_user
      when 3
        create_new_user
      when 4
        delete_current_user
      when 5
        save
      else
        #noop
    end
  end
end

def current_user_name
  return @current_user.display_name unless @current_user.nil?
  'none'
end

def select_user
  found_user = nil
  while found_user.nil? do
    print "\n###Select User###\n" +
        "Users:\n" +
        "id | display name | email\n" +
        "-------------------------\n"
    @users.each { |user|
      print "#{user.id} | #{user.display_name} | #{user.email}\n"
    }
    print 'Select a user id: '
    id = get_int
    found_user = @users.find { |user| user.id == id }
  end
  @current_user = found_user
end

def edit_user
  if @current_user.nil?
    puts 'No user selected!'
    return
  end
  selection = nil
  while selection != 3
    text = "\n###Edit User###\n" +
          "1. Change display name\n" +
          "2. Change email\n" +
          "3. Back\n"
          'Select an option: '

    selection = get_selection(text, 1, 3)

    case selection
      when 1
        change_display_name
      when 2
        change_email
      else
        #noop
    end
  end
end

def change_display_name
  puts 'Current name: ' + @current_user.display_name
  print 'Enter new display name: '
  @current_user.change_name gets.chomp
end

def change_email
  success = false
  until success do
    print 'Current email: ' + @current_user.email + "\n" +
          'Enter new email: '
    success = @current_user.change_email gets.chomp
    puts 'Bad email format!!!' unless success
  end
end

def create_new_user
  user = User.create(id: get_unique_id)
  success = false
  puts '###Create new user###'

  print 'Enter display name: '
  user.change_name gets.chomp

  until success do
    print 'Enter email: '
    success = user.change_email gets.chomp
    puts 'Bad email format!!!' unless success
  end

  @users << user
  @current_user = user
  puts 'New user created!!!'
end

def get_unique_id
  @users[-1].id + 1
end

def delete_current_user
  return if @current_user.nil?
  @users.delete_if do |user|
    user == @current_user
  end
  @current_user = nil
end

def save
  Database.save(@users)
end

def get_selection(text, range_min, range_max)
  text = text || ''
  selection = nil
  while selection.nil? || !selection.between?(range_min, range_max) do
    print text
    selection = get_int
  end
  selection
end

def get_int
  Integer(gets)
rescue ArgumentError
  nil
end





start