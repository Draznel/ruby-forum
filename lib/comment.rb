# Comment class
class Comment
  attr_reader :id, :author_id, :content, :date

  def initialize(id, author_id, content, date)
    @id = id
    @author_id = author_id
    @content = content
    @date = date
  end

  def self.create(hash)
    this = Comment.new(nil, nil, nil, nil)
    hash.each { |key, value| this.instance_variable_set("@#{key}", value) }
    this
  end

  def applicable_for_delete?(id, user)
    same_id?(id) && can_delete?(user)
  end

  def same_id?(id)
    @id == id
  end

  def can_delete?(user)
    author?(user) || UserRole.powered?(user.role)
  end

  def author?(user)
    @author_id == user.id
  end
end
