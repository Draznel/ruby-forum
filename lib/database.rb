Dir['./lib/**/*.rb'].each { |file| require file }
require './lib/file_utils'

# Database module
module Database
  private

  @paths = {
    Forum: './lib/db/forum.yml',
    ForumSection: './lib/db/forum_sections.yml',
    ForumTopic: './lib/db/forum_topics.yml',
    User: './lib/db/users.yml',
    Message: './lib/db/messages.yml',
    Comment: './lib/db/comments.yml',
    Vote: './lib/db/votes.yml'
  }
  def self.get_path_for(clazz)
    key = @paths.keys.find { |path| path.to_s == clazz.to_s }
    @paths[key] || ''
  end

  public

  def self.load(clazz)
    path = Database.get_path_for clazz
    valid_path?(path) ? FileUtils.load_yaml(path) : []
  end

  def self.save(list)
    path = Database.get_path_for(list[0].class)
    FileUtils.save(path, list) if valid_path? path
  end

  def self.valid_path?(path)
    # !path.nil? &&
    path != ''
  end
end
