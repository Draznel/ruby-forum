Dir['./lib/**/*.rb'].each { |file| require file }
require 'yaml'

# File utils class
module FileUtils
  def self.load_yaml(filepath)
    YAML.load_file(filepath)
  rescue ArgumentError => err
    puts "Could not parse YAML: #{err.message}"
  end

  def self.save(filepath, obj)
    File.open(filepath, 'w') { |file| file.write obj.to_yaml }
  end
end
