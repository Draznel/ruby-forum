require './lib/vote'
require './lib/user'
require './lib/user_role'

# Forum topic class
class ForumTopic
  attr_reader :id, :forum_section_id, :title, :creator_id,
              :creation_date, :comments, :votes

  def initialize(id)
    @id = id
    @comments = []
    @votes = []
  end

  def self.create(hash)
    this = ForumTopic.new(nil)
    hash.each { |key, value| this.instance_variable_set("@#{key}", value) }
    this
  end

  def add_comment(comment)
    @comments.push comment
  end

  def delete_comment(comment_id, user)
    @comments.delete_if do |comment|
      comment.applicable_for_delete?(comment_id, user)
    end
  end

  def comments_amount
    @comments.size
  end

  def votes
    upvotes - downvotes
  end

  def vote_list
    @votes
  end

  def total_votes
    @votes.length
  end

  def upvotes
    @votes.count { |vote| vote.type == '+' }
  end

  def downvotes
    @votes.count { |vote| vote.type == '-' }
  end

  def upvote(user)
    vote(user, '+')
  end

  def downvote(user)
    vote(user, '-')
  end

  def vote(user, type)
    return unless Vote.valid_type type
    if contains_vote? user
      vote = user_vote(user)
      vote.change_type(type)
    else
      @votes.push Vote.new(@id, user.id, 0, type)
    end
  end

  def contains_vote?(user)
    @votes.any? { |vote| vote.owner_id == user.id }
  end

  def user_vote(user)
    @votes.find { |vote| vote.owner_id == user.id }
  end
end
