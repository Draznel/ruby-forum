# Message class
class Message
  attr_reader :id, :thread_id, :topic, :sender_id,
              :recipient_id, :contents, :creation_date, :is_read
end
