require './lib/user_credentials'
require './lib/user_role'

# User class
class User
  attr_reader :id, :credentials, :role, :registration_date

  def initialize(id, credentials, role, registration_date)
    @id = id
    @credentials = credentials
    @role = role
    @registration_date = registration_date
  end

  def self.create(hash)
    this = User.new(nil, UserCredentials.new(nil, nil, nil), nil, nil)
    hash.each do |key, value|
      this.update_instance_variable(key, value)
    end
    this
  end

  def update_instance_variable(key, value)
    instance_variable_defined?("@#{key}") &&
      instance_variable_set("@#{key}", value)
    user_credentials.instance_variable_defined?("@#{key}") &&
      user_credentials.instance_variable_set("@#{key}", value)
  end

  def user_credentials
    @credentials
  end

  def display_name
    @credentials.display_name
  end

  def password
    @credentials.password
  end

  def email
    @credentials.email
  end

  def change_name(name)
    @credentials.change_name(name)
  end

  def change_password(password)
    @credentials.change_password(password)
  end

  def change_email(email)
    @credentials.change_email(email)
  end

  def change_role(role)
    @role = role if UserRole.valid? role
  end

  def ==(other)
    state == other.state
  end

  protected

  def state
    [@id, display_name, password, email, @role, @registration_date]
  end
end
