# Class representing user's display name, password and email (credentials)
class UserCredentials
  VALID_EMAIL_REGEX = /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i

  attr_reader :display_name, :password, :email

  def initialize(display_name, password, email)
    @display_name = display_name
    @password = password
    @email = email
  end

  def change_name(name)
    @display_name = name
  end

  def change_password(password)
    @password = password
  end

  def change_email(email)
    valid_email = email =~ VALID_EMAIL_REGEX
    @email = email if valid_email
    valid_email
  end
end
