# Enumeration for user role
module UserRole
  USER = 'USER'
  MOD = 'MOD'
  ADMIN = 'ADMIN'

  def self.powered?(role)
    role == UserRole::MOD || role == UserRole::ADMIN
  end

  def self.valid?(role)
    [UserRole::USER, UserRole::MOD, UserRole::ADMIN].include? role
  end
end
