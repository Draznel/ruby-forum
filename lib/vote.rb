# Vote class
class Vote
  attr_reader :id, :owner_id, :forum_topic_id, :type

  def initialize(id, owner_id, forum_topic_id, type)
    @id = id
    @owner_id = owner_id
    @forum_topic_id = forum_topic_id
    @type = type
  end

  def change_type(type)
    @type = type if Vote.valid_type type
  end

  def self.valid_type(type)
    type == '+' || type == '-'
  end
end
