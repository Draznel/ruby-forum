require 'comment'
require 'user'

RSpec.describe Comment do
  context 'general' do
    before(:each) do
      @comment = Comment.create(id: 0, author_id: 0, content: 'Hello')
      @user0 = User.create(id: 0)
      @user1 = User.create(id: 1)
    end
    it 'should have same id' do
      expect(@comment.same_id?(0)).to be_truthy
    end
    it 'should not have same id' do
      expect(@comment.same_id?(1)).to be_falsy
    end
    it 'should have same author' do
      expect(@comment.author?(@user0)).to be_truthy
    end
    it 'should not have same author' do
      expect(@comment.author?(@user1)).to be_falsy
    end
    it 'should allow same author to delete comment' do
      expect(@comment.can_delete?(@user0)).to be_truthy
    end
    it 'should not allow user to delete not his own comment' do
      expect(@comment.can_delete?(@user1)).to be_falsy
    end
    it 'should allow a powered role to delete not his own comment' do
      @user1.change_role('MOD')
      expect(@comment.can_delete?(@user1)).to be_truthy
    end
  end
end
