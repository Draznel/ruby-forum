require 'database'
require 'forum'
require 'forum_section'
require 'forum_topic'
require 'user'
require 'user_credentials'
require 'message'
require 'comment'
require 'vote'
require 'file_utils'

require 'rspec/expectations'
RSpec::Matchers.define :be_same_users_as_in_file do |expected|
  match do |actual|
    FileUtils.load_yaml(expected) == actual
  end
end
RSpec.describe Database do
  context 'loading' do
    it 'should load forum' do
      forum = Database.load(Forum)[0]
      expect(forum).to be_a Forum
    end
    it 'should load forum sections' do
      forum_sections = Database.load ForumSection
      expect(forum_sections[0]).to be_a ForumSection
    end
    it 'should load forum topics' do
      forum_topics = Database.load ForumTopic
      expect(forum_topics[0]).to be_a ForumTopic
    end
    it 'should load users' do
      users = Database.load User
      expect(users[0]).to be_a User
    end
    it 'should load messages' do
      messages = Database.load Message
      expect(messages[0]).to be_a Message
    end
    it 'should load comments' do
      comments = Database.load Comment
      expect(comments[0]).to be_a Comment
    end
    it 'should load votes' do
      votes = Database.load Vote
      expect(votes[0]).to be_a Vote
    end
  end
  context 'mocked loading' do
    it 'should return forum with data from file' do
      location = './spec/res/forum.yml'
      allow(Database).to receive(:get_path_for).and_return location

      forum = Database.load(Forum)[0]

      expect(forum.motd).to eq 'foo'
    end
    it 'should return users with data from file' do
      location = './spec/res/users.yml'
      allow(Database).to receive(:get_path_for).and_return location

      users = Database.load(User)

      expect(users).to be_same_users_as_in_file(location)
    end
  end
  context 'mocked saving' do
    before(:each) do
      @user0 = User.create(id: 0, display_name: 'adminas', role: 'ADMIN')
      @user1 = User.create(id: 1, display_name: 'modas', role: 'MOD')
      @user2 = User.create(id: 2, display_name: 'useris', role: 'USER')
      @users = [@user0, @user1, @user2]
    end
    it 'should save users to a file' do
      generated_location = './spec/res/generated/users.yml'
      allow(Database).to receive(:get_path_for).and_return generated_location

      Database.save(@users)
      loaded_users_list = Database.load(User)

      expect(loaded_users_list).to eq(@users)
    end
  end
end
