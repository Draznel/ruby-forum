require 'file_utils'
require 'rspec/expectations'

RSpec::Matchers.define :have_same_contents_as do |expected|
  match do |actual|
    actual_file = File.open(actual)
    expected_file = File.open(expected)
    actual_contents = actual_file.read
    expected_contents = expected_file.read
    actual_contents == expected_contents
  end
end
RSpec.describe FileUtils do
  context 'should load' do
    it 'yaml files' do
      users_list = FileUtils.load_yaml('spec/res/users.yml')
      expect(users_list).not_to be_nil
    end
    it 'without raising ArgumentError' do
      # RSpec.Expectations.configuration
      #   .warn_about_potential_false_positives = false
      filepath = 'non-existant-file.foo'

      allow(YAML).to receive(:load_file).with(filepath).and_raise(ArgumentError)

      expect { FileUtils.load_yaml(filepath) }
        .not_to raise_error
    end
  end
  context 'should save' do
    it 'objects as yaml' do
      user = User.create(id: 0, display_name: 'foo', password: 'pass',
                         email: 'foo@bar.com', role: UserRole::MOD,
                         registration_date: '2015-07-10T12:00:20')
      location = 'spec/res/generated/temp.yml'

      FileUtils.save(location, user)
      expect(location).to have_same_contents_as(
        'spec/res/expected_file_utils_saving_file.yml')
    end
  end
end
