require 'forum_topic'
require 'comment'
require 'user'
require 'user_role'

require 'rspec/expectations'

RSpec::Matchers.define :have_votes_symbols do |expected|
  match do |actual|
    result = ''
    actual.vote_list.each { |vote| result += vote.type }
    result == expected
  end
end

RSpec.describe ForumTopic do
  context 'with comments' do
    before(:each) do
      @forum_topic = ForumTopic.create(id: 0)
      @comment = Comment.create(id: 0, author_id: 0)
      @user = User.create(id: 0, role: UserRole::USER)
      @other_user = User.create(id: 1, role: UserRole::USER)
    end
    it 'should correctly set values with hash' do
      expect(@forum_topic.id).to eq 0
    end
    it 'should have no comments on creation' do
      expect(@forum_topic.comments_amount).to eq 0
    end
    it 'should add new comment to the end of the comment list' do
      @forum_topic.add_comment(@comment)

      expect(@forum_topic.comments)
        .to satisfy { |comments| comments[-1] == @comment }
    end
    it 'should remove a comment' do
      expect do
        @forum_topic.add_comment(@comment)
        @forum_topic.delete_comment(0, @user)
      end.not_to change { @forum_topic.comments_amount }
    end
    it 'should not allow user to remove other user\'s comment' do
      @forum_topic.add_comment(@comment)
      @forum_topic.delete_comment(0, @other_user)

      expect(@forum_topic.comments)
        .to contain_exactly(@comment)
    end
    it 'should allow MOD to remove other user\'s comment' do
      @other_user.change_role UserRole::MOD
      expect do
        @forum_topic.add_comment(@comment)
        @forum_topic.delete_comment(0, @other_user)
      end.not_to change { @forum_topic.comments_amount }
    end
    it 'should allow ADMIN to remove other user\'s comment' do
      @other_user.change_role UserRole::ADMIN

      @forum_topic.add_comment(@comment)
      @forum_topic.delete_comment(0, @other_user)

      expect(@forum_topic.comments).to match_array([])
    end
  end

  context 'with votes'do
    before(:each) do
      @forum_topic = ForumTopic.new(0)
      @user0 = User.create(id: 0, role: UserRole::USER)
      @user1 = User.create(id: 1, role: UserRole::USER)
      @user2 = User.create(id: 2, role: UserRole::USER)
    end
    it 'should have no votes on creation' do
      expect(@forum_topic.total_votes).to eq 0
    end
    it 'should add a correct upvote' do
      @forum_topic.upvote(@user0)
      last_vote = @forum_topic.vote_list[-1]
      expect(Vote.valid_type last_vote.type).to eq true
    end
    it 'should add a correct downvote' do
      @forum_topic.downvote(@user0)
      last_vote = @forum_topic.vote_list[-1]
      expect(Vote.valid_type last_vote.type).to eq true
    end
    it 'should count votes' do
      expect do
        @forum_topic.upvote(@user0)
        @forum_topic.downvote(@user0)
        @forum_topic.downvote(@user1)
        @forum_topic.upvote(@user2)
      end.to change { @forum_topic.total_votes }.by(3)
    end
    it 'should count upvotes' do
      expect do
        @forum_topic.upvote(@user0)
        @forum_topic.upvote(@user1)
      end.to change { @forum_topic.votes }.by(2)
    end
    it 'should count downvotes' do
      expect do
        @forum_topic.downvote(@user0)
        @forum_topic.downvote(@user1)
      end.to change { @forum_topic.votes }.by(-2)
    end
    it 'should show correct amount of upvotes' do
      @forum_topic.upvote(@user0)
      @forum_topic.upvote(@user1)
      @forum_topic.downvote(@user0)

      expect(@forum_topic.upvotes).to eq 1
    end
    it 'should show correct amount of upvotes' do
      @forum_topic.downvote(@user0)
      @forum_topic.downvote(@user0)
      @forum_topic.downvote(@user1)

      expect(@forum_topic.downvotes).to eq 2
    end
    it 'should rewrite older vote if user already voted' do
      expect do
        @forum_topic.upvote(@user0)
        @forum_topic.downvote(@user0)
      end.to change { @forum_topic.votes }.by(-1)
    end
    it 'should rewrite older votes of multiple users' do
      expect do
        @forum_topic.upvote(@user0)
        @forum_topic.upvote(@user1)
        @forum_topic.downvote(@user0)
      end.not_to change { @forum_topic.votes }
    end
    it 'should not change position of vote after the downvote' do
      @forum_topic.upvote(@user0)
      @forum_topic.upvote(@user1)
      @forum_topic.upvote(@user2)
      @forum_topic.downvote(@user1)
      expect(@forum_topic).to have_votes_symbols('+-+')
    end
    it "should contain user's vote" do
      @forum_topic.upvote(@user0)
      expect(@forum_topic.contains_vote? @user0).to be_truthy
    end
    it "should not contain user's vote" do
      @forum_topic.upvote(@user0)
      expect(@forum_topic.contains_vote? @user1).to be_falsy
    end
  end
end
