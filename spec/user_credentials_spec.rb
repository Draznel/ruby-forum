require 'user_credentials'

RSpec.describe UserCredentials do
  before(:each) do
    @credentials = UserCredentials.new('name', 'pass', 'foo@bar.com')
  end
  context 'general' do
    it 'should change display name' do
      @credentials.change_name 'foo'
      expect(@credentials.display_name).to eq 'foo'
    end
    it 'should change password' do
      @credentials.change_password 'secret'
      expect(@credentials.password).to eq 'secret'
    end
    it 'should change email' do
      @credentials.change_email 'test@test.com'
      expect(@credentials.email).to eq 'test@test.com'
    end
    it 'should not change email to an invalid one' do
      @credentials.change_email 'invalid_email.com'
      expect(@credentials.email).to eq 'foo@bar.com'
    end
  end
end
