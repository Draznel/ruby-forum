require 'user'
require 'user_credentials'

RSpec.describe User do
  context 'creation' do
    it 'should correctly create user from hash' do
      user = User.create(id: 0, display_name: 'useris',
                         password: 'pass', email: 'user@foo.com',
                         role: UserRole::USER,
                         registration_date: '2015-02-02T12:00:00')

      credentials = UserCredentials.new('useris', 'pass', 'user@foo.com')
      expected_user = User.new(0, credentials, UserRole::USER,
                               '2015-02-02T12:00:00')

      expect(user).to eq expected_user
    end
  end
  context 'general' do
    before(:each) do
      @user = User.create(id: 0, display_name: 'username', email: 'foo@bar.com')
    end
    it "should change user's display name" do
      @user.change_name 'foo'
      expect(@user.display_name).to eq 'foo'
    end
    it "should change user's password" do
      @user.change_password 'secret'
      expect(@user.password).to eq 'secret'
    end
    it "should change user's email" do
      @user.change_email 'test@test.com'
      expect(@user.email).to eq 'test@test.com'
    end
    it "should not change user's email to an invalid one" do
      @user.change_email 'invalid_email.com'
      expect(@user.email).to eq 'foo@bar.com'
    end
  end
end
