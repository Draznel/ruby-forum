require 'vote'

RSpec.describe Vote do
  before(:each) do
    @vote = Vote.new(0, 0, 0, '+')
  end
  context 'creation' do
    it 'should create vote object' do
      expect(@vote).to_not eq nil
    end
  end
  context 'general' do
    it 'should set new vote type' do
      @vote.change_type '-'
      expect(@vote.type).to eq '-'
    end
    it 'should not set new vote type' do
      @vote.change_type '.'
      expect(@vote.type).to eq '+'
    end
  end
  context 'validity' do
    it 'should show + to be valid' do
      expect(Vote.valid_type '+').to eq true
    end
    it 'should show - to be valid' do
      expect(Vote.valid_type '-').to eq true
    end
    it 'should show * to be invalid' do
      expect(Vote.valid_type '*').to eq false
    end
    it "should show 'a' to be invalid" do
      expect(Vote.valid_type 'a').to eq false
    end
  end
end
