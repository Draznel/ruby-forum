1) Užduoties tema: Forumas
2, 3)
Sistemos savybės:
  vartotojas:
    #(if rolė in USER/MOD/ADMIN)
      gali užsiregistruoti prie sistemos
      gali prisijungti prie sistemos
      gali paprašyti sistemos priminti slaptažodį
      gali atsijungti nuo sistemos
      gali matyti prisijungusių žmonių skaičių
      gali matyti savo duomenis(išskyrus id, slaptažodį)
      gali keisti savo el.paštą, slaptažodį
      gali matyti MOTD(message of the day)
      gali matyti užsiregistravusius vartotojus
      gali ieškoti užsiregistravusio vartotojo
      gali matyti užsiregistravusio vartotojo duomenis(išskyrus id, slaptažodį, el.paštą)
      gali kurti forumo temą
      gali ieškoti forumo temos
      gali matyti savo sukurtas forumo temas
      gali upvote'inti/downvote'inti forumo temą
      gali rašyti komentarą forumo temos viduje
      gali redaguoti savo komentarą forumo temos viduje
      gali siųsti privačią žinutę kitam vartotojui
      gali matyti gautas privačias žinutes
      gali matyti išsiųstas privačias žinutes
      gali matyti privačios žinutės turinį
      gali trinti privačią žinutę

    #(if rolė in MOD/ADMIN)
      gali trinti forumo pokalbį
      gali trinti komentarą forumo pokalbio viduje

    #(if rolė in ADMIN)
      gali kurti forumo skiltį
      gali keisti kitų vartotojų rolę(išskyrus ADMIN)
      gali trinti kitų vartotojų paskyras
      gali keisti MOTD(message of the day)

Sistemos esybės:
  Vartotojas
    * turi id(automatiškai generuojamas)
    * turi display_name
    * turi slaptažodį
    * turi el.paštą
    * turi rolę(USER/MOD/ADMIN)
    * turi užsiregistravimo datą
    * turi sukurtų forumo temų skaičių
    * turi parašytų komentarų skaičių

  Forumas
    * turi forumo skilčių sąrašą
    * turi užsiregistravusių vartotojų sąrašą
    * turi privačių žinučių sąrašą
    * turi MOTD(message of the day)

  Forumo skiltis
    * turi id
    * turi pavadinimą
    * turi minimalią reikiamą rolę
    * turi forumo temų sąrašą

  Forumo tema
    * turi id
    * turi forumo skilties id
    * turi pavadinimą
    * turi kūrėjo id
    * turi sukūrimo datą
    * turi balsų sąrašą(upvote/downvote)
    * turi komentarų sąrašą

  Balsas
    * turi id
    * turi sąvininko id
    * turi forumo temos id
    * turi tipą("+", "-")

  Komentaras
    * turi id
    * turi autoriaus id
    * turi turinį
    * turi sukūrimo datą

  Privati žinutė
    * turi id
    * turi gijosId
    * turi temą
    * turi siuntėjo id
    * turi gavėjo id
    * turi turinį
    * turi sukūrimo datą
    * turi ar perskaityta žinutė flag'ą

4) 1 žmogus. Ernestas Mitkus. Studento nr.: S1311019
